package com.conti_online.continentaldrivingsimulation;

/**
 * Created by Siddharth Mathur on 8/11/2016.
 */
public class JSONParser {

    public JSONParser(){}

    public void parseInterIds(String curr){
        for(int x=0; x<curr.length(); x++){
            if(curr.substring(x, x+12).equals("RoadSegments")) break;
            if(curr.substring(x, x+14).equals("IntersectionID")) MainActivity.interIds.add(Integer.parseInt(curr.substring(curr.indexOf(':', x+14)+1, curr.indexOf(',', x+16))));
            if(curr.charAt(x)=='X') MainActivity.intersectionX.add(Integer.parseInt(curr.substring(x+3, curr.indexOf(',', x))));
            if(curr.charAt(x)=='Y') MainActivity.intersectionY.add(Integer.parseInt(curr.substring(x+3, curr.indexOf('}', x))));
        }
    }

    public void parseSegments(String curr){
        boolean pastRoads=false;
        for(int x=0; x<curr.length(); x++){
            if(!pastRoads) {
                if (curr.substring(x, x + 12).equals("RoadSegments")) pastRoads = true;
            }
            else if(curr.substring(x, x+6).equals("Length")){
                MainActivity.segmentId.add(Integer.parseInt(curr.substring(curr.indexOf(':', x-6)+1, curr.indexOf(',', x-6))));
                MainActivity.segId++;
                MainActivity.inter1.add(Integer.parseInt(curr.substring(curr.indexOf('[', x+25)+1, curr.indexOf(',', x+29))));
                MainActivity.inter2.add(Integer.parseInt(curr.substring(curr.indexOf(',', x+29)+1, curr.indexOf(']', x+25))));
                if(curr.indexOf("]}]}", x)!=-1 && curr.indexOf("]}]}", x)<x+55) break;
            }
        }
    }

    public void parseSignals(String curr){
        for(int x=0; x<curr.length(); x++){
            if(curr.substring(x, x+8).equals("SignalID")){
                MainActivity.sigId.add(Integer.parseInt(curr.substring(curr.indexOf(':', x)+1, curr.indexOf('}', x))));
                MainActivity.intId.add(Integer.parseInt(curr.substring(curr.indexOf("IntersectionID", x-170)+16, curr.indexOf(',', curr.indexOf("IntersectionID", x-170)))));
                MainActivity.lightSegId.add(Integer.parseInt(curr.substring(curr.indexOf(':', x-7)+1, curr.indexOf(',', x-7))));
                MainActivity.lightStates.add(0);
                if(curr.charAt(x+15)==']') break;
            }
        }
    }

    public void parseDynSignals(String curr2){
        MainActivity.id=Integer.parseInt(curr2.substring(curr2.indexOf("SignalID")+10, curr2.indexOf(',', curr2.indexOf("SignalID")+10)));
        MainActivity.state=Integer.parseInt(curr2.substring(curr2.indexOf("SignalState")+13, curr2.indexOf("SignalState")+14));
    }

    public void parseCars(String curr2){
        for(int x=0; x<curr2.length()-4; x++){
            if(curr2.substring(x, x+5).equals("CarID")){
                MainActivity.carIds.add(Integer.parseInt(curr2.substring(curr2.indexOf("CarID", x)+7, curr2.indexOf(',', curr2.indexOf("CarID",x)))));
                //MainActivity.carSpeed.add(Integer.parseInt(curr2.substring(curr2.indexOf("Speed",x)+7, curr2.indexOf(',', curr2.indexOf("Speed",x)))));
                MainActivity.carSpeed.add(1);
                MainActivity.carPos.add((int)Double.parseDouble(curr2.substring(curr2.indexOf("RoadPosition",x)+14, curr2.indexOf(',', curr2.indexOf("RoadPosition",x)))));
                MainActivity.carSeg.add(Integer.parseInt(curr2.substring(curr2.indexOf("SegmentID",x)+11, curr2.indexOf(',', curr2.indexOf("SegmentID",x)))));
                MainActivity.carDir.add(Integer.parseInt(curr2.substring(curr2.indexOf("Direction",x)+11, curr2.indexOf('}', curr2.indexOf("Direction",x)))));
            }
        }
    }

    public Integer parseCarId(String curr2){
        return Integer.parseInt(curr2.substring(curr2.indexOf("CarID")+7, curr2.indexOf(',', curr2.indexOf("CarID"))));
    }

    public void parseDynCars(String curr2, int z){
        MainActivity.carSpeed.set(z, Integer.parseInt(curr2.substring(curr2.indexOf("Speed")+7, curr2.indexOf(',', curr2.indexOf("Speed")))));
        MainActivity.carSeg.set(z, Integer.parseInt(curr2.substring(curr2.indexOf("SegmentID")+11, curr2.indexOf(',', curr2.indexOf("SegmentID")))));
        MainActivity.carDir.set(z, Integer.parseInt(curr2.substring(curr2.indexOf("Direction")+11, curr2.indexOf('}', curr2.indexOf("Direction")))));
    }
}
